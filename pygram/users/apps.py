"""User app configuration."""

from django.apps import AppConfig


# Create your models here.
class UsersConfig(AppConfig):
    """User app config."""

    name = 'users'
    verbose_name = 'Users'
